//
//  CommonHelper.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/15/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
class CommonHelper {
    public static  func getThemeColor() -> UIColor {
        return UIColor(red: 68/255, green: 149/255, blue: 209/255, alpha: 1.0)
    }
    public static let baseurl = "http://www.techlogicians.com/qualhand/index.php"
    public static var pages : [Page] = []
    public static var history : [Page] = []
    public static var tree :[Page] = []
    public static var indexData :[IndexData] = []
    public static var dictionaryData :[Option] = []
    public static let mailBody = "<p><strong>Regards -</strong></p><br/><p><strong><span style=\"color: #ff0000;\">Qual</span><span style=\"color: #3366ff;\">Hand</span> Team</strong></p>"
     public static let mailSubject = "QualHand Reading History"
    public static func getpages()->[Page]{
        if(pages.count > 0){
            return pages
        }else{
            if let path = Bundle.main.path(forResource: "pages", ofType: "json") {
                do {
                    print("inseide do")
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let obj = try JSONDecoder().decode([Page].self, from: data)
                    
                    CommonHelper.pages = obj
                    return obj
                    
                    
                } catch {
                        return[]
                    
                }
            }
            
        }
        return pages
    }
    
    public static func getIndexData()->[IndexData]{
        if(indexData.count == 0){
            if let path = Bundle.main.path(forResource: "index", ofType: "json") {
                do {
                    print("inseide do")
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let obj = try JSONDecoder().decode([IndexData].self, from: data)
                    indexData = obj
                    
                    
                    
                } catch {
                    // handle error
                }
            }
        }
        
        return indexData
    }
    
    public static func getDictionaryData()->[Option]{
        if(dictionaryData.count == 0){
            if let path = Bundle.main.path(forResource: "dictonarydata", ofType: "json") {
                do {
                    print("inseide do")
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let obj = try JSONDecoder().decode([Option].self, from: data)
                    dictionaryData = obj
                    
                    
                    
                } catch {
                    // handle error
                }
            }
        }
        
        return dictionaryData
    }
    
    public static func postRequest(url :String ,params : [String:Any] ,completionBlock: @escaping (String) -> Void) {
       
        
        // create post request
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        // insert json data to the request
        request.httpBody = params.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
//                print(responseJSON)
                let responseString = String(data: data, encoding: .utf8)
                completionBlock(responseString!)
            }
        }
        
        task.resume()
        
    }
    
    public static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
