//
//  Extensions.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/16/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    
}


//extension String {
//    var html2Attributed: NSAttributedString? {
//        do {
//            guard let data = data(using: String.Encoding.utf8) else {
//                return nil
//            }
//            return try NSAttributedString(data: data,
//                                          options: [.documentType: NSAttributedString.DocumentType.html,
//                                                    .characterEncoding: String.Encoding.utf8.rawValue],
//                                          documentAttributes: nil)
//        } catch {
//            print("error: ", error)
//            return nil
//        }
//    }
//}
