//
//  AtMentionsLabel.swift
//  QualHand
//
//  Created by Mausum Nandy on 8/26/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import Foundation
import UIKit

protocol QHLabelTapDelegate: class {
    func labelWasTapped(_ word: String)
}

class QHLabel: UILabel {
    private var tapGesture: UITapGestureRecognizer = UITapGestureRecognizer()
    weak var tapDelegate: QHLabelTapDelegate?
    
    var keywords: [String] = [] // usernames to style
    var scrollView : UIScrollView = UIScrollView(frame: .zero)
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        isUserInteractionEnabled = true
        
        lineBreakMode = .byWordWrapping
        tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(handleLabelTap(recognizer:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.isEnabled = true
        addGestureRecognizer(tapGesture)
    }
    
    
    @objc func handleLabelTap(recognizer: UITapGestureRecognizer) {
        let tapLocation = recognizer.location(in: self.scrollView)
        let tapIndex = indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        print("Tap Index : \(tapIndex)")
        for keyword in keywords {
            if let ranges = self.attributedText?.rangesOf(subString: keyword) {
                for range in ranges {
                    //print("Range \(keyword) \(range.location) \(range.length)")
                    if tapIndex > range.location && (tapIndex < range.location + range.length ){
                        tapDelegate?.labelWasTapped(keyword)
                        return
                    }
                    
                }
            }
        }
    }
    
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        guard let attributedString = self.attributedText else { return -1 }
        
        let mutableAttribString = NSMutableAttributedString(attributedString: attributedString)
        // Add font so the correct range is returned for multi-line labels
        let f = UIFont(name: "TimesNewRomanPS-ItalicMT", size: 14.67)
        mutableAttribString.addAttributes([NSAttributedString.Key.font: f!], range: NSRange(location: 0, length: attributedString.length))
        
        let textStorage = NSTextStorage(attributedString: mutableAttribString)
        
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        
        let textContainer = NSTextContainer(size: frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = numberOfLines
        textContainer.lineBreakMode = lineBreakMode
        layoutManager.addTextContainer(textContainer)
        
        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}

extension NSAttributedString {
    func rangesOf(subString: String) -> [NSRange] {
        var nsRanges: [NSRange] = []
        let ranges = string.ranges(of: subString, options: .caseInsensitive, locale: nil)
        
        for range in ranges {
            //nsRanges.append(range.nsRange)
            nsRanges.append(NSRange(range,in:string))
        }
        
        return nsRanges
    }
}

extension String {
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex) ..< self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
}
//extension Range where Bound == String.Index {
//    var nsRange:NSRange {
//        return NSRange(location: self.lowerBound.encodedOffset,
//                       length: self.upperBound.encodedOffset -
//                        self.lowerBound.encodedOffset)
//    }
//}
extension StringProtocol {
    func nsRange(from range: Range<Index>) -> NSRange {
        return .init(range, in: self)
    }
}
