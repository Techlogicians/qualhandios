//
//  OptionCell.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/22/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class OptionCell: UITableViewCell {
    
    

    var data : String? {
        didSet{
            
            descriptionLabel.attributedText = data!.html2Attributed
            
            descriptionLabel.numberOfLines = 0
            descriptionLabel.sizeToFit()
        }
    }
    
     let descriptionLabel : QHLabel = {
        let label = QHLabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
       
        super.init(style: style , reuseIdentifier: reuseIdentifier)
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupViews(){
        selectionStyle = .none
        self.contentView.addSubview(descriptionLabel)
       
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        
        self.contentView.addConstraintsWithFormat("H:|-10-[v0]-5-|", views: descriptionLabel)
        self.contentView.addConstraintsWithFormat("H:|-5-[v0]-5-|", views: view)
        
        self.contentView.addConstraintsWithFormat("V:|-5-[v0]-[v1]-5-|", views: descriptionLabel,view)
//        descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5).isActive = true
//        descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 5).isActive = true
//
//        descriptionLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant:1).isActive = true
//        descriptionLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant:2).isActive = true
        
    }
}
