//
//  NoteController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/24/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit
import CoreData


class NoteController: UIViewController {
    var pageId = 0
    
    let segmentedControl: UISegmentedControl = {
        let items = ["Current Note" , "All Notes"]
        let sm =  UISegmentedControl(items : items)
        
        sm.selectedSegmentIndex = 0
        sm.addTarget(self, action: #selector(indexChanged(_:)), for: .valueChanged)
        sm.translatesAutoresizingMaskIntoConstraints =  false
        sm.layer.cornerRadius = 5.0
        sm.backgroundColor = .white
        sm.tintColor = CommonHelper.getThemeColor()
        return sm
    }()
    
    let containerView :UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    lazy var doneBtn : UIBarButtonItem = {
        let button1 =   UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.saveNote) )
        button1.tintColor = .white
        return button1
    }()
    var currentViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.view.backgroundColor = .white
        self.view.addSubview(segmentedControl)
        segmentedControl.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        segmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.view.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: segmentedControl)
        self.view.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 10).isActive = true
         self.view.addConstraintsWithFormat("H:|[v0]|", views: containerView)
        let controller = EditNoteController()
        controller.pageId = self.pageId
        self.currentViewController = controller
        
         self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChild(self.currentViewController!)
        self.addSubview(subView: self.currentViewController!.view, toView: self.containerView)
        
        
        self.navigationItem.rightBarButtonItem = doneBtn
//        self.navigationController?.navigationBar.backItem?.title = "Back"
        navigationController?.navigationBar.tintColor = .white
    }
    @objc func saveNote(){
        if let vc = currentViewController as? EditNoteController{
            let text = vc.textView.text!
            if(text != "Write Note Here"){
                createOrUpdate(text: text)
                
            }
            navigationController?.popViewController(animated: true)
            dismiss(animated: true, completion: nil)
        }
    }
   
    @objc func indexChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            let newViewController = EditNoteController()
            newViewController.pageId = self.pageId
            newViewController.view.translatesAutoresizingMaskIntoConstraints = false
            self.cycleFromViewController(oldViewController: self.currentViewController!, toViewController: newViewController)
            self.currentViewController = newViewController
            
            self.navigationItem.rightBarButtonItem = doneBtn
        } else {
            let newViewController = ListNotesController()
            newViewController.view.translatesAutoresizingMaskIntoConstraints = false
            self.cycleFromViewController(oldViewController: self.currentViewController!, toViewController: newViewController)
            self.currentViewController = newViewController
            self.navigationItem.rightBarButtonItem = nil
        }
    }

    func cycleFromViewController(oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMove(toParent: nil)
        self.addChild(newViewController)
        self.addSubview(subView: newViewController.view, toView:self.containerView)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            newViewController.view.alpha = 1
            oldViewController.view.alpha = 0
        },
                                   completion: { finished in
                                    oldViewController.view.removeFromSuperview()
                                    oldViewController.removeFromParent()
                                    newViewController.didMove(toParent: self)
        })
    }
    
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    
    
    func createOrUpdate(text:String){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "page_id = %d", argumentArray: [pageId])
        
      
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchrequest.predicate = predicate
        fetchrequest.fetchLimit = 1
        do{
            let note = try( context.fetch(fetchrequest) as! [Notes])
            if(note.count > 0){
               note[0].content = text
            }else{
                let note = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context) as! Notes
                note.page_id = Int32(pageId)
                note.content = text
            }
            
            
        }catch let err{
            print(err)
        }
        do{
            try(context.save())
        }catch let err{
            print(err)
        }
    }
    func deleteNotes()  {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        do{
             let notes = try( context.fetch(fetchrequest) as! [Notes])
            for note in notes{
                context.delete(note)
            }
        }catch let err{
            print(err)
        }
       
        
        
    }

}
