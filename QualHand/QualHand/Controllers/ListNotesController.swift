//
//  NotesController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/29/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit
import CoreData
class ListNotesController: UITableViewController {
 fileprivate let cellId = "CellId"
    var notes : [Notes] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        do{
             notes = try( context.fetch(fetchrequest) as! [Notes])

        }catch let err{
            print(err)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return notes.count
    }

 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! SubtitleTableViewCell
        let note = notes[indexPath.row]
        cell.selectionStyle = .none
        
        let page = CommonHelper.pages[Int(note.page_id-1)]
        let attstr = page.content.html2Attributed
        let title :String  = (attstr?.string.prefix(100).components(separatedBy: "\n")[0])!
        
        cell.textLabel!.text = title
        
        cell.detailTextLabel!.text = note.content
        cell.detailTextLabel!.numberOfLines = 0
        
        cell.detailTextLabel!.preferredMaxLayoutWidth = view.bounds.width
        
        cell.detailTextLabel!.setNeedsDisplay()

        return cell
    }
 

   

}
