//
//  PViewController.swift
//  QualHand
//
//  Created by Mausum Nandy on 10/21/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit
import WebKit
class PViewController: UIViewController,WKNavigationDelegate {
   
   lazy var webView : WKWebView = {
        let wv = WKWebView()
        wv.translatesAutoresizingMaskIntoConstraints = false
        wv.navigationDelegate = self
        return wv
    }()
    var dictionaryData : [Option] = []
    var page : Page?  {
        didSet{
            let pageid = "\(page?.page_id ?? 0)"
            if let url = Bundle.main.url(forResource: pageid  , withExtension: "html", subdirectory: "pages")
            {
                webView.loadFileURL(url, allowingReadAccessTo: url)
                let request = URLRequest(url: url)
                webView.load(request)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        webView.backgroundColor = .white
        self.view.addSubview(webView)
        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        webView.navigationDelegate = self
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "QualHand"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationController?.navigationBar.tintColor = .white
       
        
        dictionaryData = CommonHelper.getDictionaryData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.page?.page_id == 3 || self.page?.page_id == 2){
            CommonHelper.tree = []
        }
        if !CommonHelper.tree.contains(where: { $0.page_id == page?.page_id }) {
            CommonHelper.tree.append(page!)
        }
        let button1 =   UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(noteView) )
        button1.tintColor = .white
        let button2 = UIBarButtonItem(image: UIImage(named: "history"), style: .plain, target: self, action:  #selector(historyView))
        button2.tintColor = .white
        self.navigationItem.rightBarButtonItems = [button1,button2]
    }
    
    @objc func noteView(_ sender :  Any){
        let controller = NoteController()
        controller.pageId = page!.page_id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func historyView(){
        let controller = HistoryController()
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url
        guard url != nil else {
            decisionHandler(.allow)
            return
        }
        
        if url!.description.lowercased().starts(with: "http://") ||
            url!.description.lowercased().starts(with: "https://")  {
            decisionHandler(.cancel)
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else if url!.description.lowercased().starts(with: "file://"){
            decisionHandler(.allow)
            let arr = url!.absoluteString.split(separator:"/")
            if !arr.last!.contains(".html"){
              let  destinaton = arr.last!.replacingOccurrences(of: "%20", with: " ")
                if let page = Int(destinaton){
                   goto(pageid: page)
                }else{
                    showdef(word: destinaton)
                }
            }
            
        }
    }
    private func showdef(word: String){
        let  theOne = dictionaryData.filter{
            $0.title.range(of: word, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
        }
        
        
        if(theOne.count > 0 ){
            let alert = UIAlertController(title: word.firstCapitalized, message: theOne[0].detail?.html2Attributed?.string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    private func goto(pageid : Int){
        if(pageid == 27){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let tabBarController = appDelegate.window?.rootViewController as! UITabBarController
            tabBarController.selectedIndex = 0
        }else{
            let controller = PViewController()
            controller.page = CommonHelper.pages[pageid-1]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
