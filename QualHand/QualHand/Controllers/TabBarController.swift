//
//  TabBarController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/15/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController ,UITabBarControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.tabBar.tintColor = CommonHelper.getThemeColor()
         
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let indexTab = UINavigationController(rootViewController : IndexController())
        let taboneImage = UIImage(named: "index")?.withRenderingMode(.alwaysTemplate)
        let tabBarItemOne = UITabBarItem(title: "Index", image: taboneImage, tag: 0)
        indexTab.tabBarItem = tabBarItemOne
        
        let homeTab = UINavigationController(rootViewController : HomeController())
        let tabImageTwo = UIImage(named: "home")?.withRenderingMode(.alwaysTemplate)
        let tabBarItemTwo = UITabBarItem(title: "Home", image: tabImageTwo, tag: 1)
        homeTab.tabBarItem = tabBarItemTwo
        
        let shareTab = UINavigationController(rootViewController : ShareController())
        let tabImageThree = UIImage(named: "share")?.withRenderingMode(.alwaysTemplate)
        let tabBarItemThree = UITabBarItem(title: "Share", image: tabImageThree, tag: 1)
        shareTab.tabBarItem = tabBarItemThree
        
        let moreTab = UINavigationController(rootViewController : MoreController())
        let tabImageFour = UIImage(named: "more")?.withRenderingMode(.alwaysTemplate)
        let tabBarItemFour = UITabBarItem(title: "More", image: tabImageFour, tag: 1)
        moreTab.tabBarItem = tabBarItemFour
        
        self.viewControllers = [indexTab, homeTab,shareTab,moreTab]
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let fromView: UIView = tabBarController.selectedViewController!.view
        let toView  : UIView = viewController.view
        if fromView == toView {
            return false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0, options: [.curveEaseInOut], completion: nil)
        }
        return true
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        self.viewControllers?.forEach({ (vc) in
            if let w = vc as? UINavigationController{
                w.popToRootViewController(animated: true)
            }
            
        })
    }

}
