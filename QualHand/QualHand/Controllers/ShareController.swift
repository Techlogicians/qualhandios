//
//  ShareController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/31/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit
import WebKit
import MessageUI
import CoreData
class ShareController: UIViewController,MFMailComposeViewControllerDelegate {
var html = ""
    let webview : WKWebView = {
        let wv = WKWebView(frame: .zero)
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()
    lazy var send : UIBarButtonItem = {
        let button1 = UIBarButtonItem(image: UIImage(named: "send"), style: .plain, target: self, action:  #selector(sendFeedbackButtonTapped))
        
        button1.tintColor = .white
        return button1
    }()
    var  pdfFilePath = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.view.addSubview(webview)
        view.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: webview)
        view.addConstraintsWithFormat("V:|-10-[v0]-10-|", views: webview)
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Share"
       
        
    }
    var notes : [Notes] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        do{
            notes = try( context.fetch(fetchrequest) as! [Notes])
            
        }catch let err{
            print(err)
        }
        loadData()
    }
    
    func loadData() {
        if(CommonHelper.tree.count > 0){
             self.navigationItem.rightBarButtonItem = send
            html =  "<html><body style=\"padding-left: 25px;padding-right: 25px;padding-top: 15px; padding-bottom: 25px;margin: 10px;\">"
            
            CommonHelper.tree.forEach { (page) in
                html = "\(html) </br></br></br> </b>Page : \(page.page_id)</br> </b> \(page.content)"
                if(page.page_id > 8){
                    html = "\(html)<p style=\"font-size: 12pt;font-family: 'Calibri';\"><b>Quality Strategies.</b></p><br/>\(page.foot_note!)"
                }
            }
            html = "\(html)<br/><b>NOTES:</b><br/>"
            notes.forEach { (note) in
               let page = CommonHelper.pages[Int(note.page_id-1)]
                let attstr = page.content.html2Attributed
                let title :String  = (attstr?.string.prefix(100).components(separatedBy: "\n")[0])!
                html = "\(html)<p>\(title)<br/><span style=\"padding:10;\">\(String(describing: note.content!))</span><p/>"
            }
            
            html = "\(html)</body></html>"
            
            webview.loadHTMLString(html, baseURL: nil)  
        }
        
       
    }
    @objc func sendFeedbackButtonTapped(_ sender: Any) {
        let mailComposeViewController = configureMailComposer()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            print("Can't send email")
        }
        
    }
    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self

        mailComposeVC.setSubject(CommonHelper.mailSubject)
        mailComposeVC.setMessageBody(CommonHelper.mailBody, isHTML: true)
        pdfFilePath = self.webview.exportAsPdfFromWebView()
        

        if let fileData = NSData(contentsOfFile: pdfFilePath){
            mailComposeVC.addAttachmentData(fileData as Data, mimeType: "application/pdf", fileName: "qualhand.pdf")
        }
        return mailComposeVC
    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: pdfFilePath) {
                // Delete file
                try fileManager.removeItem(atPath: pdfFilePath)
            } else {
                print("File does not exist")
            }
            
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension WKWebView {
    
    // Call this function when WKWebView finish loading
    func exportAsPdfFromWebView() -> String {
        let pdfData = createPdfFile(printFormatter: self.viewPrintFormatter())
        return self.saveWebViewPdf(data: pdfData)
    }
    
    func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {

        
        let pageSize = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printableSize = pageSize.insetBy(dx: 0, dy: 0)
        
        let printPageRenderer = UIPrintPageRenderer()
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        printPageRenderer.setValue(NSValue(cgRect: pageSize), forKey: "paperRect")
        printPageRenderer.setValue(NSValue(cgRect: printableSize), forKey: "printableRect")
       
        return printPageRenderer.generatePdfData()
    }
    
    // Save pdf file in document directory
    func saveWebViewPdf(data: NSMutableData) -> String {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("webViewPdf.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
}

extension UIPrintPageRenderer {
    
    func generatePdfData() -> NSMutableData {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, self.paperRect, nil)
        self.prepare(forDrawingPages: NSMakeRange(0, self.numberOfPages))
        let printRect = UIGraphicsGetPDFContextBounds()
        for pdfPage in 0..<self.numberOfPages {
            UIGraphicsBeginPDFPage()
            self.drawPage(at: pdfPage, in: printRect)
        }
        UIGraphicsEndPDFContext();
        return pdfData
    }
}
