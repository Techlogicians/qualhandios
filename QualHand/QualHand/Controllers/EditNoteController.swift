//
//  EditNoteController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/29/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit
import CoreData
class EditNoteController: UIViewController {
    
    
    var pageId :Int = 0{
        didSet{
            getNote()
        }
    }
    let textView : UITextView = {
        let tv = UITextView(frame: .zero)
        tv.translatesAutoresizingMaskIntoConstraints = false
    
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(textView)
         view.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: textView)
         view.addConstraintsWithFormat("V:|-10-[v0]-10-|", views: textView)
      
        
    }
    
    func getNote(){
        let delegate = UIApplication.shared.delegate as! AppDelegate
          let predicate = NSPredicate(format: "page_id = %d", argumentArray: [pageId])
        
        let context = delegate.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchrequest.predicate = predicate
        fetchrequest.fetchLimit = 1
        do{
            let note = try( context.fetch(fetchrequest) as! [Notes])
            if(note.count > 0){
                 textView.text = note[0].content
            }else{
                textView.text = "Write Note Here"
            }
           
           
        }catch let err{
            print(err)
        }
    }

    
}
