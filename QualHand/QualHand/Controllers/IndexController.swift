//
//  IndexController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/15/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class IndexController: UITableViewController,UISearchBarDelegate {
    
    
    let cellId = "reuseIdentifier"
    var data : [IndexData] = []
    
    let searchBar = UISearchBar()
     var isSerachBarHidden = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Index"
        searchBar.delegate = self
        
        let button1 =   UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchButtonAction) )
        button1.tintColor = .white
        self.navigationItem.rightBarButtonItem  = button1
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        data = CommonHelper.getIndexData()
        filtered = data
    }
    @objc func searchButtonAction(){
       
        if(isSerachBarHidden){
            self.navigationItem.titleView = searchBar
            searchBar.becomeFirstResponder()
            isSerachBarHidden = false
        }else{
            isSerachBarHidden = true
             self.navigationItem.titleView = nil
            self.navigationItem.title = "Index"
            filtered = data
            searchBar.resignFirstResponder()
            searchBar.text = ""
           
        }
        
        
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
       
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filtered.count
    }

 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = filtered[indexPath.row].title
        
        

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let i = filtered[indexPath.item]
        if(i.page_id == 26){
            let controller = DictonaryController()
            
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            let controller = PViewController()
            print("page \(i.page_id)")
            controller.page = CommonHelper.pages[i.page_id-1]
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
  
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText == ""){
            filtered = data
        }else{
            filtered = data.filter{
                $0.title.range(of: searchText, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
            }
        }
        
    }
    var filtered = [IndexData]() {
        didSet{
            self.tableView.reloadData()
        }
    }
}
