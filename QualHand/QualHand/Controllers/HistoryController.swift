//
//  HistoryController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/23/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class HistoryController: UITableViewController {
    fileprivate let cellId = "CellId"
    fileprivate var pages : [Page] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "History"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationController?.navigationBar.tintColor = .white
         tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        self.pages = CommonHelper.tree
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return pages.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! SubtitleTableViewCell
       let page = pages[indexPath.row]
//        cell.textLabel!.text = "Page \(page.page_id)"
        
        cell.detailTextLabel!.attributedText = page.content.html2Attributed
//        cell.detailTextLabel!.numberOfLines = 0
//
//        cell.detailTextLabel!.preferredMaxLayoutWidth = view.bounds.width
//
//        cell.detailTextLabel!.setNeedsDisplay()
        return cell
       

        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
            let controller = PViewController()
            controller.page = self.pages[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
       
    }
   

}
class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
