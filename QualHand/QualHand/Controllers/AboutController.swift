//
//  HomeController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/15/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class AboutController: UIViewController {
    
    var page : Page? {
        didSet{
            descriptionlabel.attributedText = page?.content.html2Attributed
        }
    }
    let scrollView : UIScrollView = {
        let sv = UIScrollView(frame: .zero)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
        
    }()
    
    let descriptionlabel : UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    let holderView : UIView = UIView(frame: .zero)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "About QualHand"
        page = CommonHelper.pages[24]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupviews()
        self.view.layoutIfNeeded()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let hg = descriptionlabel.frame.origin.y
        let wd = descriptionlabel.frame.height
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: hg+wd+25)
        scrollView.isScrollEnabled = true
    }
    func setupviews(){
        
        self.view.addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        holderView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(holderView)
        
        holderView.leadingAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        
        holderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        holderView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        holderView.heightAnchor.constraint(equalToConstant: 20000).isActive = true
        
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        holderView.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: holderView.centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        imageView.topAnchor.constraint(equalTo: holderView.topAnchor, constant: 10).isActive = true
        
        
        holderView.addSubview(descriptionlabel)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: descriptionlabel)
        descriptionlabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10).isActive = true
       
        
        
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
