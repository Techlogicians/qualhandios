//
//  PageController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/20/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class PageController: UIViewController, UITableViewDelegate, UITableViewDataSource,QHLabelTapDelegate {
    
    fileprivate let cellId : String = "cellId"
    var page : Page? {
        didSet{
            descriptionlabel.attributedText = page?.content.html2Attributed
            descriptionlabel.keywords = page!.keywords
            print(page!.keywords)
            noteLabel.attributedText = page?.foot_note?.html2Attributed
            noteLabel.sizeToFit()
            twodimArray = page!.options
            
            self.tableview.reloadData()
            CommonHelper.history.append(self.page!)
            pagevisit()
        }
    }
    let scrollView : UIScrollView = {
        let sv = UIScrollView(frame: .zero)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
        
    }()
    let holderView : UIView = UIView(frame: .zero)
    lazy var descriptionlabel : QHLabel = {
        let label = QHLabel(frame: .zero)
        label.scrollView = scrollView
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    let tableview :  UITableView = {
        let tv =  UITableView(frame: .zero, style: .plain)
        //tv.isScrollEnabled = false;
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    let noteLabel : UILabel = {
        let label = UILabel(frame: .zero)
        
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        descriptionlabel.tapDelegate = self
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "QualHand"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationController?.navigationBar.tintColor = .white
        
        self.tableview.register(OptionCell.self, forCellReuseIdentifier: cellId)
        //tableview.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
        tableview.tableFooterView = UIView()
        tableview.isScrollEnabled = false
        dictionaryData = CommonHelper.getDictionaryData()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupViews()
        if(self.page?.page_id == 3 || self.page?.page_id == 2){
            CommonHelper.tree = []
        }
        if !CommonHelper.tree.contains(where: { $0.page_id == page?.page_id }) {
            CommonHelper.tree.append(page!)
        }
        
        
    }
    
    func setupViews() {
        self.view.addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        holderView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(holderView)
        
        holderView.leadingAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        
        holderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        holderView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        holderView.heightAnchor.constraint(equalToConstant: 20000).isActive = true
        
        holderView.addSubview(descriptionlabel)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: descriptionlabel)
        descriptionlabel.topAnchor.constraint(equalTo: holderView.topAnchor, constant: 10).isActive = true
        
        
        
        holderView.addSubview(tableview)
        holderView.addConstraintsWithFormat("H:|-20-[v0]-10-|", views: tableview)
        if((page?.options.count)! > 1){
            tableview.heightAnchor.constraint(equalToConstant: CGFloat((page?.options.count)! * 120)).isActive = true
            tableview.topAnchor.constraint(equalTo: descriptionlabel.bottomAnchor, constant: 5).isActive = true
        }else{
             tableview.heightAnchor.constraint(equalToConstant: 45).isActive = true
            tableview.topAnchor.constraint(equalTo: descriptionlabel.bottomAnchor, constant: 1).isActive = true
        }
        holderView.addSubview(noteLabel)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: noteLabel)
        noteLabel.topAnchor.constraint(equalTo: tableview.bottomAnchor, constant: 25).isActive = true
       
        let button1 =   UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(noteView) )
        button1.tintColor = .white
        let button2 = UIBarButtonItem(image: UIImage(named: "history"), style: .plain, target: self, action:  #selector(historyView))
        button2.tintColor = .white
        self.navigationItem.rightBarButtonItems = [button1,button2]
        
        
        
        
        
        
    }
    @objc func noteView(_ sender :  Any){
        let controller = NoteController()
        controller.pageId = page!.page_id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func historyView(){
        let controller = HistoryController()
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let hg = holderView.subviews.last?.frame.origin.y
        let hgt = holderView.subviews.last?.frame.height
        scrollView.contentSize = CGSize(width: self.view.bounds.width, height: hg!+hgt!+20)
        scrollView.isScrollEnabled = true
    }
    
    var twodimArray  : [Option] = []
    var dictionaryData : [Option] = []
    var theOne : [Option] = []
    func labelWasTapped(_ word: String) {
        print(word)
        
        let  theOne = dictionaryData.filter{
            $0.title.range(of: word, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
        }
        
        
        if(theOne.count > 0 ){
            let alert = UIAlertController(title: word.firstCapitalized, message: theOne[0].detail?.html2Attributed?.string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
       
    }
}

extension PageController {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return  (page?.options.count)!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (page?.options[section].isExpened)!{
            return  1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OptionCell
        cell.data = page?.options[indexPath.section].detail
        cell.descriptionLabel.tapDelegate = self
        cell.descriptionLabel.keywords = page!.keywords
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y:0 , width:tableView.frame.width, height: 55))
        view.backgroundColor = .white
        view.tag = section
        view.isUserInteractionEnabled = true
        
        let titleLabel = UILabel(frame: CGRect(x:  view.bounds.minX+10, y: 10, width:view.bounds.width - 50 , height: 50))
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.attributedText = page?.options[section].title.html2Attributed
//        titleLabel.font =  UIFont.boldSystemFont(ofSize: 16.0)
        titleLabel.numberOfLines = 0
        titleLabel.backgroundColor = .clear
        titleLabel.preferredMaxLayoutWidth = view.bounds.width
        
        titleLabel.setNeedsDisplay()
        
        view.addSubview(titleLabel)
        
        if(page?.options[section].detail != ""){
            let infoButton = UIButton(type: .infoLight)
            infoButton.frame = CGRect(x: view.bounds.maxX-20, y:10 , width: 20, height:20)
            infoButton.addTarget(self, action: #selector(infoBtnAction(_:)), for: .touchUpInside)
            infoButton.tag = section
            
            view.addSubview(infoButton)
        }
        
        
        let gesture = UITapGestureRecognizer(target: self, action:#selector(tabExpand))
        
        view.addGestureRecognizer(gesture)
        
        return view
    }
    
    @objc func infoBtnAction(_ sender : UIButton){
        
        let section = sender.tag
        
        var indexPaths = [IndexPath]()
        let indexpath = IndexPath(row: 0, section: section)
        indexPaths.append(indexpath)
        let isExpanded = page?.options[section].isExpened
        page?.options[section].isExpened = !isExpanded!
        if(isExpanded!){
            self.tableview.deleteRows(at: indexPaths, with: .fade)
            
            
        }else{
            self.tableview.insertRows(at: indexPaths, with: .fade)
            for (index, option) in (page?.options.enumerated())! {
                if(index != section && option.isExpened){
                    var ips = [IndexPath]()
                    let ip = IndexPath(row: 0, section: index)
                    ips.append(ip)
                    page?.options[index].isExpened = false
                    self.tableview.deleteRows(at: ips, with: .fade)
                }

            }
        }
        
        
    }
    
    
    @objc func tabExpand(_ sender: UITapGestureRecognizer)  {
        let view = sender.view
        let section = view!.tag
        if(page?.options[section].destination_page_id == 27){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let tabBarController = appDelegate.window?.rootViewController as! UITabBarController
            tabBarController.selectedIndex = 0
        }else{
            let controller = PageController()
            controller.page = CommonHelper.pages[(page?.options[section].destination_page_id)!-1]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func pagevisit()  {
        if CommonHelper.isInternetAvailable(){
            let deviceID = UIDevice.current.identifierForVendor?.uuidString
            
            let dict: [String:Any] =
                [
                    "device_id": "\(deviceID!)",
                    "page_id" : "\(String(describing: page!.page_id))"
            ]
           
            CommonHelper.postRequest(url: CommonHelper.baseurl, params:dict) {
                (_) in
//                print(output)
            }
        }else{
           // print("not available")
        }
      
    }
}



extension StringProtocol {
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
}
