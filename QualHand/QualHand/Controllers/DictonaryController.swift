//
//  DictonaryController.swift
//  QualHand
//
//  Created by Mausum Nandy on 8/1/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class DictonaryController: UITableViewController,UISearchBarDelegate {
    fileprivate let cellId = "Cell"
    var options : [Option] = []
    var filtered = [Option](){
        didSet{
            self.tableView.reloadData()
        }
    }
    let searchBar = UISearchBar()
    var isSerachBarHidden = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(OptionCell.self, forCellReuseIdentifier: cellId)
        tableView.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
        tableView.tableFooterView = UIView()
       
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Dictionary"
        searchBar.delegate = self
        
        let button1 =   UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchButtonAction) )
        button1.tintColor = .white
        self.navigationItem.rightBarButtonItem  = button1
        options = CommonHelper.getDictionaryData()
        filtered = options
       
    }
    @objc func searchButtonAction(){
        
        if(isSerachBarHidden){
            self.navigationItem.titleView = searchBar
            searchBar.becomeFirstResponder()
            isSerachBarHidden = false
        }else{
            isSerachBarHidden = true
            self.navigationItem.titleView = nil
            self.navigationItem.title = "Dictionary"
            filtered = options
            searchBar.resignFirstResponder()
            searchBar.text = ""
            
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText == ""){
            filtered = options
        }else{
            filtered = options.filter{
                $0.title.range(of: searchText, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
            }
        }
        
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
       
        return filtered.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (filtered[section].isExpened){
            return  1
        }else{
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OptionCell
        cell.data = filtered[indexPath.section].detail
        return cell

        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y:0 , width:tableView.frame.width, height: 55))
        view.backgroundColor = .white
        view.tag = section
        view.isUserInteractionEnabled = true
        
        let titleLabel = UILabel(frame: CGRect(x:  view.bounds.minX+10, y: 10, width:view.bounds.width - 50 , height: 50))
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.text = filtered[section].title
        titleLabel.numberOfLines = 0
        titleLabel.backgroundColor = .clear
        titleLabel.preferredMaxLayoutWidth = view.bounds.width
        
        titleLabel.setNeedsDisplay()
        
        view.addSubview(titleLabel)
        
        
        let infoButton = UIButton(type: .infoLight)
        infoButton.frame = CGRect(x: view.bounds.maxX-30, y:10 , width: 20, height:20)
        infoButton.addTarget(self, action: #selector(infoBtnAction(_:)), for: .touchUpInside)
        infoButton.tag = section
        view.addSubview(infoButton)
        
        let gesture = UITapGestureRecognizer(target: self, action:#selector(tabExpand))
        
        view.addGestureRecognizer(gesture)
        
        return view
    }
    @objc func infoBtnAction(_ sender : UIButton){
        
        let section = sender.tag
       
        var indexPaths = [IndexPath]()
        let indexpath = IndexPath(row: 0, section: section)
        indexPaths.append(indexpath)
        let isExpanded = filtered[section].isExpened
        filtered[section].isExpened = !isExpanded
        if(isExpanded){
            self.tableView.deleteRows(at: indexPaths, with: .none)
            
        }else{
            self.tableView.insertRows(at: indexPaths, with: .none)
            for (index, option) in (filtered.enumerated()) {
                if(index != section && option.isExpened){
                    var ips = [IndexPath]()
                    let ip = IndexPath(row: 0, section: index)
                    ips.append(ip)
                    filtered[index].isExpened = false
                    self.tableView.deleteRows(at: ips, with: .none)
                }
                
            }
            
        }
    }
    
    
    @objc func tabExpand(_ sender: UITapGestureRecognizer)  {

    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
