//
//  HomeController.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/15/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    var page : Page? {
        didSet{
            descriptionlabel.attributedText = page?.content.html2Attributed
          
            btn1.setAttributedTitle(page?.options[0].title.html2Attributed, for: .normal)
            //btn1.setTitle(page?.options[0].title, for: .normal)
            btn1.titleLabel?.numberOfLines = 1
            btn1.titleLabel?.adjustsFontSizeToFitWidth = true
            btn1.titleLabel!.lineBreakMode = .byClipping //NSLineBreakByClipping;
           
            btn2.setAttributedTitle(page?.options[1].title.html2Attributed, for: .normal)
            
            btn2.titleLabel?.numberOfLines = 0
            btn2.titleLabel?.adjustsFontSizeToFitWidth = true
            btn2.titleLabel!.lineBreakMode = .byClipping
        }
    }
    let scrollView : UIScrollView = {
        let sv = UIScrollView(frame: .zero)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
        
    }()
    
    let descriptionlabel : UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    lazy var btn1 : UIButton = {
        let button = UIButton(type: .system)
        
        button.titleLabel?.textColor = CommonHelper.getThemeColor()
      
        button.addTarget(self, action: #selector(alreadyKnowAction), for: .touchUpInside)
        return button
    }()
    lazy var btn2 : UIButton = {
        let button = UIButton(type: .system)
        
        button.titleLabel?.textColor = CommonHelper.getThemeColor()
        
        button.addTarget(self, action: #selector(showmeaction), for: .touchUpInside)
        return button
    }()
    @objc func alreadyKnowAction(){
        let controller = PViewController()//PageController()
        controller.page = CommonHelper.pages[1]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func showmeaction(){
        let controller = PViewController()
        controller.page = CommonHelper.pages[2]
        self.navigationController?.pushViewController(controller, animated: true)
    }
      let holderView : UIView = UIView(frame: .zero)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barTintColor = CommonHelper.getThemeColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Home"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        let pages: [Page] = CommonHelper.getpages()
        self.page = pages[0]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupviews()
        self.view.layoutIfNeeded()
        CommonHelper.tree = []
      
    }
  
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let hg = btn2.frame.origin.y
        let wd = btn2.frame.height
    
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: hg+wd+25)
        scrollView.isScrollEnabled = true
    }
    func setupviews(){
        
        self.view.addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
      
        
        holderView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(holderView)
        
        holderView.leadingAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        
        holderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        holderView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        holderView.heightAnchor.constraint(equalToConstant: 20000).isActive = true
       
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        holderView.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: holderView.centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.topAnchor.constraint(equalTo: holderView.topAnchor, constant: 10).isActive = true
        
        
        holderView.addSubview(descriptionlabel)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: descriptionlabel)
        descriptionlabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10).isActive = true
        holderView.addSubview(btn1)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: btn1)
        btn1.topAnchor.constraint(equalTo: descriptionlabel.bottomAnchor, constant: 10).isActive = true
        btn1.heightAnchor.constraint(equalToConstant: 55).isActive = true
       
        holderView.addSubview(btn2)
        holderView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: btn2)
        btn2.topAnchor.constraint(equalTo: btn1.bottomAnchor, constant: 10).isActive = true
        btn2.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        
        
      
    }
    
    
}
