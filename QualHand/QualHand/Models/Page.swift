//
//  Page.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/23/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import Foundation

class Page  :  Codable{
    var  page_id        : Int
    var  content        : String
    var  options        : [Option]
    var  foot_note      : String?
    var  keywords       : [String]
    
    required init(from decoder: Decoder) throws{
        let container         =  try decoder.container(keyedBy: CodingKeys.self)
        self.page_id          = try container.decodeIfPresent(Int.self, forKey: .page_id) ?? 0
        self.content          = try container.decodeIfPresent(String.self, forKey: .content)!
        self.foot_note        = try container.decodeIfPresent(String.self, forKey: .foot_note) ?? ""
        self.options          =  try container.decodeIfPresent([Option].self, forKey: .options) ?? []
        self.keywords          =  try container.decodeIfPresent([String].self, forKey: .keywords) ?? []
    }
}

class Option: Codable {
    var title :  String
    var detail : String?
    var isExpened : Bool = false
    
    var destination_page_id : Int?
    
    required init(from decoder: Decoder) throws{
        let container         =  try decoder.container(keyedBy: CodingKeys.self)
        self.destination_page_id          = try container.decodeIfPresent(Int.self, forKey: .destination_page_id) ?? 0
        self.title          = try container.decodeIfPresent(String.self, forKey: .title)!
        self.detail        = try container.decodeIfPresent(String.self, forKey: .detail) ?? ""
        self.isExpened  = false
    }
}
