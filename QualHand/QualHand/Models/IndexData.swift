//
//  IndexData.swift
//  QualHand
//
//  Created by Mausum Nandy on 8/1/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//

import Foundation
class IndexData: Codable {
    var title :  String
    var page_id : Int

    
   
    
    required init(from decoder: Decoder) throws{
        let container         =  try decoder.container(keyedBy: CodingKeys.self)
        self.page_id          = try container.decodeIfPresent(Int.self, forKey: .page_id) ?? 0
        self.title          = try container.decodeIfPresent(String.self, forKey: .title)!
        
    }
}
