//
//  Notes+CoreDataProperties.swift
//  QualHand
//
//  Created by Mausum Nandy on 7/30/19.
//  Copyright © 2019 Mausum Nandy. All rights reserved.
//
//

import Foundation
import CoreData


extension Notes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notes> {
        return NSFetchRequest<Notes>(entityName: "Notes")
    }

    @NSManaged public var content: String?
    @NSManaged public var page_id: Int32
    @NSManaged public var id: Int16

}
